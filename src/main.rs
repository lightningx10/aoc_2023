use std::{env, error::Error, fs};

mod lib {
    pub mod day1;
    pub mod day10;
    pub mod day11;
    pub mod day2;
    pub mod day3;
    pub mod day4;
    pub mod day5;
    pub mod day6;
    pub mod day7;
    pub mod day8;
    pub mod day9;
}

const NUM_DAYS: usize = 11;

const FNS: [(fn(&str), fn(&str)); NUM_DAYS] = [
    (lib::day1::day1_p1, lib::day1::day1_p2),
    (lib::day2::day2_p1, lib::day2::day2_p2),
    (lib::day3::day3_p1, lib::day3::day3_p2),
    (lib::day4::day4_p1, lib::day4::day4_p2),
    (lib::day5::day5_p1, lib::day5::day5_p2),
    (lib::day6::day6_p1, lib::day6::day6_p2),
    (lib::day7::day7_p1, lib::day7::day7_p2),
    (lib::day8::day8_p1, lib::day8::day8_p2),
    (lib::day9::day9_p1, lib::day9::day9_p2),
    (lib::day10::day10_p1, lib::day10::day10_p2),
    (lib::day11::day11_p1, lib::day11::day11_p2),
];

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        let day = usize::from_str_radix(args.get(1).ok_or("Failed to get the 2nd arg")?, 10)?;
        if day > NUM_DAYS {
            eprintln!("Day out of range!");
            return Ok(());
        }
        let fname = format!("inputs/day{}", day);
        let input = fs::read_to_string(fname)?;
        let (p1, p2) = FNS[(day - 1) as usize];
        p1(&input);
        p2(&input);
    } else {
        println!(
            "WARNING: This may take a while to run on some days if I've decided to brute force"
        );
        for i in 0..NUM_DAYS {
            println!("=== Day {}: ===", i + 1);
            let fname = format!("inputs/day{}", i + 1);
            let input = fs::read_to_string(fname)?;
            let (p1, p2) = FNS[i];
            p1(&input);
            p2(&input);
        }
    }
    Ok(())
}
