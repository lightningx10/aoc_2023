use std::collections::{HashSet, HashMap};

pub fn day4_p1(input: &str) {
    let mut scores: Vec<Option<usize>> = Vec::new();
    for line in input.lines() {
        let (_card_str, nums) = line.split_once(":").unwrap();
        let (winning_nums_str, our_nums_str) = nums.split_once("|").unwrap();
        let winning_nums: HashSet<usize> = winning_nums_str
            .split(" ")
            .filter_map(|s| usize::from_str_radix(s, 10).ok())
            .collect();
        let our_nums: Vec<usize> = our_nums_str
            .split(" ")
            .filter_map(|s| usize::from_str_radix(s, 10).ok())
            .collect();

        let mut game_score: Option<usize> = None;
        for num in our_nums {
            if winning_nums.contains(&num) {
                game_score = match game_score {
                    Some(n) => Some(n * 2),
                    None => Some(1)
                }
            }
        }
        scores.push(game_score);
    }
    let sum: usize = scores.into_iter().filter_map(|x| x).fold(0, |acc, n| acc + n);
    println!("Day 4 Part 1: {}", sum);
}

pub fn day4_p2(input: &str) {
    let mut num_cards: HashMap<usize, usize> = HashMap::new();
    for line in input.lines() {
        let (card_str, nums) = line.split_once(":").unwrap();
        let card_no = usize::from_str_radix(&card_str.chars().filter(|c| c.is_numeric()).collect::<String>(), 10).unwrap();
        let (winning_nums_str, our_nums_str) = nums.split_once("|").unwrap();
        let winning_nums: HashSet<usize> = winning_nums_str
            .split(" ")
            .filter_map(|s| usize::from_str_radix(s, 10).ok())
            .collect();
        let our_nums: Vec<usize> = our_nums_str
            .split(" ")
            .filter_map(|s| usize::from_str_radix(s, 10).ok())
            .collect();

        let mut num_winnings: usize = 0;
        for num in our_nums {
            if winning_nums.contains(&num) {
                num_winnings += 1;
            }
        }
        let num_copies_current = num_cards.get(&card_no).unwrap_or(&0).to_owned() + 1;
        num_cards.insert(card_no, num_copies_current);

        let lbnd = card_no+1;
        let ubnd = card_no + num_winnings;
        for i in lbnd..=ubnd {
            let mut num_i = num_cards.get(&i).unwrap_or(&0).to_owned();
            num_i += num_copies_current;
            num_cards.insert(i, num_i);
        }
    }
    let sum: usize = num_cards.iter().fold(0, |acc, (_, n)| acc + *n);
    println!("Day 4 Part 2: {}", sum);
}
