pub fn day9_p1(input: &str) {
    let mut predicted_values: Vec<i64> = Vec::new();
    for line in input.lines() {
        let mut lasts: Vec<i64> = Vec::new();
        let mut nums: Vec<i64> = line
            .split(" ")
            .filter_map(|s| i64::from_str_radix(s, 10).ok())
            .collect();
        while nums.iter().any(|&n| n != 0) {
            for i in 1..nums.len() {
                nums[i - 1] = nums[i] - nums[i - 1];
            }
            lasts.push(nums.remove(nums.len() - 1));
        }
        predicted_values.push(lasts.iter().fold(0, |acc, &n| acc + n));
    }
    let ans = predicted_values.iter().fold(0, |acc, &n| acc + n);
    println!("Day 9 Part 1: {}", ans);
}

pub fn day9_p2(input: &str) {
    let mut predicted_values: Vec<i64> = Vec::new();
    for line in input.lines() {
        let mut firsts: Vec<i64> = Vec::new();
        let mut nums: Vec<i64> = line
            .split(" ")
            .filter_map(|s| i64::from_str_radix(s, 10).ok())
            .collect();
        firsts.push(nums[0]);
        while nums.iter().any(|&n| n != 0) {
            for i in 1..nums.len() {
                nums[i - 1] = nums[i] - nums[i - 1];
            }
            firsts.push(nums[0]);
            nums.remove(nums.len() - 1);
            //println!("nums: {:?}", nums);
        }
        //println!("firsts: {:?}", firsts);
        firsts.reverse();
        let mut pred = 0;
        for i in 1..firsts.len() {
            pred = firsts[i] - pred;
        }
        predicted_values.push(pred);
    }
    let ans = predicted_values.iter().fold(0, |acc, &n| acc + n);
    println!("Day 9 Part 2: {}", ans);
}
