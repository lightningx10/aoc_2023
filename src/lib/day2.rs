use regex::Regex;

enum Colour {
    Red,
    Green,
    Blue
}

pub fn day2_p1(input: &str) {
    const MAX_RED: usize = 12;
    const MAX_GREEN: usize = 13;
    const MAX_BLUE: usize = 14;

    let red_re = Regex::new("red").unwrap();
    let green_re = Regex::new("green").unwrap();
    let blue_re = Regex::new("blue").unwrap();

    let mut s = 0;

    for line in input.lines() {
        let colon_i: usize = line.find(':').expect("Invalid input line given");
        let (l, r) = line.split_at(colon_i);
        let game_id = l.chars()
            .filter(|c| *c >= '0' && *c <= '9')
            .filter_map(|c| c.to_digit(10).and_then(|n| Some(n as usize)))
            .rev()
            .enumerate()
            .fold(0 as usize, |acc, (i, n)| acc + (10 as usize).pow(i as u32) * n);
        let mut game_valid = true;
        for section in r.split(";") {
            for colour_decl in section.split(",") {
                let n = colour_decl.chars()
                    .filter(|c| *c >= '0' && *c <= '9')
                    .filter_map(|c| c.to_digit(10).and_then(|n| Some(n as usize)))
                    .rev()
                    .enumerate()
                    .fold(0 as usize, |acc, (i, n)| acc + (10 as usize).pow(i as u32) * n);
                let col = if red_re.is_match(colour_decl) {
                    Colour::Red
                } else if green_re.is_match(colour_decl) {
                    Colour::Green
                } else if blue_re.is_match(colour_decl) {
                    Colour::Blue
                } else { panic!() };

                let is_valid: bool = match col {
                    Colour::Red => n <= MAX_RED,
                    Colour::Green => n <= MAX_GREEN,
                    Colour::Blue => n <= MAX_BLUE
                };
                game_valid &= is_valid;
            }
        }
        if game_valid {
            s += game_id;
        }
    }
    println!("Day 2 Part 1: {}", s);
}

pub fn day2_p2(input: &str) {
    let red_re = Regex::new("red").unwrap();
    let green_re = Regex::new("green").unwrap();
    let blue_re = Regex::new("blue").unwrap();

    let mut s = 0;

    for line in input.lines() {
        let colon_i: usize = line.find(':').expect("Invalid input line given");
        let (_, r) = line.split_at(colon_i);

        let mut min_red = 0;
        let mut min_green = 0;
        let mut min_blue = 0;

        for section in r.split(";") {
            for colour_decl in section.split(",") {
                let n = colour_decl.chars()
                    .filter(|c| *c >= '0' && *c <= '9')
                    .filter_map(|c| c.to_digit(10).and_then(|n| Some(n as usize)))
                    .rev()
                    .enumerate()
                    .fold(0 as usize, |acc, (i, n)| acc + (10 as usize).pow(i as u32) * n);
                let col = if red_re.is_match(colour_decl) {
                    Colour::Red
                } else if green_re.is_match(colour_decl) {
                    Colour::Green
                } else if blue_re.is_match(colour_decl) {
                    Colour::Blue
                } else { panic!() };
                match col {
                    Colour::Red => {
                        if n > min_red {
                            min_red = n;
                        }
                    }
                    Colour::Green => {
                        if n > min_green {
                            min_green = n;
                        }
                    }
                    Colour::Blue => {
                        if n > min_blue {
                            min_blue = n;
                        }
                    }
                }
            }
        }
        let power = min_red * min_green * min_blue;
        s += power;
    }
    println!("Day 2 Part 2: {}", s);
}
