use regex::Regex;

pub fn day1_p1(input: &str) {
    let mut n = 0;
    for line in input.lines() {
        let ns: Vec<u32> = line.chars().filter_map(|c| c.to_digit(10)).collect();
        n += ns.get(0).unwrap() * 10 + ns.get(ns.len() - 1).unwrap();
    }
    println!("Day 1 Part 1: {}", n)
}

pub fn day1_p2(input: &str) {
    let mut n = 0;
    let mut leftmost: Option<(usize, usize)>;
    let mut rightmost: Option<(usize, usize)>;
    let res: [Regex; 10] = [
        Regex::new(r"zero").unwrap(),
        Regex::new(r"one").unwrap(),
        Regex::new(r"two").unwrap(),
        Regex::new(r"three").unwrap(),
        Regex::new(r"four").unwrap(),
        Regex::new(r"five").unwrap(),
        Regex::new(r"six").unwrap(),
        Regex::new(r"seven").unwrap(),
        Regex::new(r"eight").unwrap(),
        Regex::new(r"nine").unwrap(),
    ];
    for line in input.lines() {
        leftmost = None;
        rightmost = None;
        for (i, re) in res.iter().enumerate() {
            for m in re.find_iter(line) {
                let id = m.start();
                if leftmost.is_none() || leftmost.is_some_and(|(pos, _)| id < pos) {
                    leftmost = Some((id, i));
                }
                if rightmost.is_none() || rightmost.is_some_and(|(pos, _)| id > pos) {
                    rightmost = Some((id, i));
                }
            }
        }
        let otherns: Vec<(usize, usize)> = line.chars().enumerate().filter_map(|(id, c)| c.to_digit(10).and_then(|n| Some((id, n as usize)))).collect();
        for (otheri, othern) in otherns.iter() {
            if leftmost.is_none() || leftmost.is_some_and(|(i, _)| *otheri < i) {
                leftmost = Some((*otheri, *othern));
            }
            if rightmost.is_none() || rightmost.is_some_and(|(i, _)| *otheri > i) {
                rightmost = Some((*otheri, *othern));
            }
        }
        n += leftmost.unwrap().1 * 10 + rightmost.unwrap().1;
    }
    println!("Day 1 Part 2: {}", n)
}
