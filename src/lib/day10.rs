#[derive(Eq, PartialEq, Clone, Copy, Debug)]
enum Tile {
    Vertical,
    Horizontal,
    BendNE,
    BendNW,
    BendSW,
    BendSE,
    Ground,
    Start,
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
enum Velocity {
    North,
    East,
    South,
    West,
}

impl From<char> for Tile {
    fn from(c: char) -> Self {
        match c {
            '|' => Tile::Vertical,
            '-' => Tile::Horizontal,
            'L' => Tile::BendNE,
            'J' => Tile::BendNW,
            '7' => Tile::BendSW,
            'F' => Tile::BendSE,
            '.' => Tile::Ground,
            'S' => Tile::Start,
            _ => Tile::Ground,
        }
    }
}

pub fn day10_p1(input: &str) {
    let tile_grid: Vec<Vec<Tile>> = input
        .lines()
        .map(|line| line.chars().map(Tile::from).collect())
        .collect();
    let mut current_pos: (usize, usize) = (0, 0);
    let mut current_tile;
    let line_len = tile_grid[0].len();
    for (i, tile) in tile_grid.iter().flatten().enumerate() {
        if *tile == Tile::Start {
            current_pos = (i % line_len, i / line_len);
        }
    }
    let north = if current_pos.1 == 0 {
        None
    } else {
        tile_grid
            .get(current_pos.1 - 1)
            .and_then(|l| l.get(current_pos.0))
    };
    let south = tile_grid
        .get(current_pos.1 + 1)
        .and_then(|l| l.get(current_pos.0));
    let east = tile_grid
        .get(current_pos.1)
        .and_then(|l| l.get(current_pos.0 + 1));
    let west = if current_pos.0 == 0 {
        None
    } else {
        tile_grid
            .get(current_pos.1)
            .and_then(|l| l.get(current_pos.0 - 1))
    };
    let mut current_velocity: Velocity;
    match (north, south, east, west) {
        (Some(&t), _, _, _) if t == Tile::Vertical || t == Tile::BendSE || t == Tile::BendSW => {
            current_tile = t;
            current_pos = (current_pos.0, current_pos.1 - 1);
            current_velocity = Velocity::North;
        }
        (_, Some(&t), _, _) if t == Tile::Vertical || t == Tile::BendNE || t == Tile::BendNW => {
            current_tile = t;
            current_pos = (current_pos.0, current_pos.1 + 1);
            current_velocity = Velocity::South;
        }
        (_, _, Some(&t), _) if t == Tile::Horizontal || t == Tile::BendNW || t == Tile::BendSW => {
            current_tile = t;
            current_pos = (current_pos.0 + 1, current_pos.1);
            current_velocity = Velocity::East;
        }
        (_, _, _, Some(&t)) if t == Tile::Horizontal || t == Tile::BendNE || t == Tile::BendSE => {
            current_tile = t;
            current_pos = (current_pos.0 - 1, current_pos.1);
            current_velocity = Velocity::West;
        }
        _ => panic!("We should never get here!"),
    }
    let mut dist = 1;
    while current_tile != Tile::Start {
        match (current_tile, current_velocity) {
            (Tile::Vertical, Velocity::North) => current_pos = (current_pos.0, current_pos.1 - 1),
            (Tile::Vertical, Velocity::South) => current_pos = (current_pos.0, current_pos.1 + 1),
            (Tile::Vertical, _) => panic!("This should never happen!"),
            (Tile::Horizontal, Velocity::East) => current_pos = (current_pos.0 + 1, current_pos.1),
            (Tile::Horizontal, Velocity::West) => current_pos = (current_pos.0 - 1, current_pos.1),
            (Tile::Horizontal, _) => panic!("This should never happen!"),
            (Tile::BendNE, Velocity::South) => {
                current_pos = (current_pos.0 + 1, current_pos.1);
                current_velocity = Velocity::East;
            }
            (Tile::BendNE, Velocity::West) => {
                current_pos = (current_pos.0, current_pos.1 - 1);
                current_velocity = Velocity::North;
            }
            (Tile::BendNE, _) => panic!("This should never happen!"),
            (Tile::BendNW, Velocity::South) => {
                current_pos = (current_pos.0 - 1, current_pos.1);
                current_velocity = Velocity::West;
            }
            (Tile::BendNW, Velocity::East) => {
                current_pos = (current_pos.0, current_pos.1 - 1);
                current_velocity = Velocity::North;
            }
            (Tile::BendNW, _) => panic!("This should never happen!"),
            (Tile::BendSW, Velocity::North) => {
                current_pos = (current_pos.0 - 1, current_pos.1);
                current_velocity = Velocity::West;
            }
            (Tile::BendSW, Velocity::East) => {
                current_pos = (current_pos.0, current_pos.1 + 1);
                current_velocity = Velocity::South;
            }
            (Tile::BendSW, _) => panic!("This should never happen!"),
            (Tile::BendSE, Velocity::North) => {
                current_pos = (current_pos.0 + 1, current_pos.1);
                current_velocity = Velocity::East;
            }
            (Tile::BendSE, Velocity::West) => {
                current_pos = (current_pos.0, current_pos.1 + 1);
                current_velocity = Velocity::South;
            }
            (Tile::BendSE, _) => panic!("This should never happen!"),
            (Tile::Ground, _) => panic!("This should never happen!"),
            (Tile::Start, _) => panic!("We should have left the loop before this point"),
        }
        current_tile = tile_grid
            .get(current_pos.1)
            .unwrap()
            .get(current_pos.0)
            .unwrap()
            .to_owned();
        dist += 1;
    }
    println!("Day 10 Part 1: {}", dist / 2);
}

pub fn day10_p2(input: &str) {
    let tile_grid: Vec<Vec<Tile>> = input
        .lines()
        .map(|line| line.chars().map(Tile::from).collect())
        .collect();
    let mut loop_tiles: Vec<Vec<Tile>> =
        vec![vec![Tile::Ground; tile_grid[0].len()]; tile_grid.len()];
    let mut current_pos: (usize, usize) = (0, 0);
    let mut current_tile;
    let line_len = tile_grid[0].len();
    for (i, tile) in tile_grid.iter().flatten().enumerate() {
        if *tile == Tile::Start {
            current_pos = (i % line_len, i / line_len);
        }
    }
    let north = if current_pos.1 == 0 {
        None
    } else {
        tile_grid.get(current_pos.1 - 1).and_then(|l| {
            l.get(current_pos.0)
                .and_then(|&t| if t == Tile::Ground { None } else { Some(t) })
        })
    };
    let south = tile_grid.get(current_pos.1 + 1).and_then(|l| {
        l.get(current_pos.0)
            .and_then(|&t| if t == Tile::Ground { None } else { Some(t) })
    });
    let east = tile_grid.get(current_pos.1).and_then(|l| {
        l.get(current_pos.0 + 1)
            .and_then(|&t| if t == Tile::Ground { None } else { Some(t) })
    });
    let west = if current_pos.0 == 0 {
        None
    } else {
        tile_grid.get(current_pos.1).and_then(|l| {
            l.get(current_pos.0 - 1)
                .and_then(|&t| if t == Tile::Ground { None } else { Some(t) })
        })
    };

    match (north, south, east, west) {
        (Some(_), Some(_), _, _) => loop_tiles[current_pos.1][current_pos.0] = Tile::Vertical,
        (Some(_), _, Some(_), _) => loop_tiles[current_pos.1][current_pos.0] = Tile::BendNE,
        (Some(_), _, _, Some(_)) => loop_tiles[current_pos.1][current_pos.0] = Tile::BendNW,
        (_, Some(_), Some(_), _) => loop_tiles[current_pos.1][current_pos.0] = Tile::BendSE,
        (_, Some(_), _, Some(_)) => loop_tiles[current_pos.1][current_pos.0] = Tile::BendSW,
        (_, _, Some(_), Some(_)) => loop_tiles[current_pos.1][current_pos.0] = Tile::Horizontal,
        _ => panic!("We could not find two entrances to the starting point"),
    }

    let mut current_velocity: Velocity;
    match (north, south, east, west) {
        (Some(t), _, _, _) if t == Tile::Vertical || t == Tile::BendSE || t == Tile::BendSW => {
            current_tile = t;
            current_pos = (current_pos.0, current_pos.1 - 1);
            current_velocity = Velocity::North;
        }
        (_, Some(t), _, _) if t == Tile::Vertical || t == Tile::BendNE || t == Tile::BendNW => {
            current_tile = t;
            current_pos = (current_pos.0, current_pos.1 + 1);
            current_velocity = Velocity::South;
        }
        (_, _, Some(t), _) if t == Tile::Horizontal || t == Tile::BendNW || t == Tile::BendSW => {
            current_tile = t;
            current_pos = (current_pos.0 + 1, current_pos.1);
            current_velocity = Velocity::East;
        }
        (_, _, _, Some(t)) if t == Tile::Horizontal || t == Tile::BendNE || t == Tile::BendSE => {
            current_tile = t;
            current_pos = (current_pos.0 - 1, current_pos.1);
            current_velocity = Velocity::West;
        }
        _ => panic!("We should never get here!"),
    }

    loop_tiles[current_pos.1][current_pos.0] = current_tile;

    while current_tile != Tile::Start {
        match (current_tile, current_velocity) {
            (Tile::Vertical, Velocity::North) => current_pos = (current_pos.0, current_pos.1 - 1),
            (Tile::Vertical, Velocity::South) => current_pos = (current_pos.0, current_pos.1 + 1),
            (Tile::Vertical, _) => panic!("This should never happen!"),
            (Tile::Horizontal, Velocity::East) => current_pos = (current_pos.0 + 1, current_pos.1),
            (Tile::Horizontal, Velocity::West) => current_pos = (current_pos.0 - 1, current_pos.1),
            (Tile::Horizontal, _) => panic!("This should never happen!"),
            (Tile::BendNE, Velocity::South) => {
                current_pos = (current_pos.0 + 1, current_pos.1);
                current_velocity = Velocity::East;
            }
            (Tile::BendNE, Velocity::West) => {
                current_pos = (current_pos.0, current_pos.1 - 1);
                current_velocity = Velocity::North;
            }
            (Tile::BendNE, _) => panic!("This should never happen!"),
            (Tile::BendNW, Velocity::South) => {
                current_pos = (current_pos.0 - 1, current_pos.1);
                current_velocity = Velocity::West;
            }
            (Tile::BendNW, Velocity::East) => {
                current_pos = (current_pos.0, current_pos.1 - 1);
                current_velocity = Velocity::North;
            }
            (Tile::BendNW, _) => panic!("This should never happen!"),
            (Tile::BendSW, Velocity::North) => {
                current_pos = (current_pos.0 - 1, current_pos.1);
                current_velocity = Velocity::West;
            }
            (Tile::BendSW, Velocity::East) => {
                current_pos = (current_pos.0, current_pos.1 + 1);
                current_velocity = Velocity::South;
            }
            (Tile::BendSW, _) => panic!("This should never happen!"),
            (Tile::BendSE, Velocity::North) => {
                current_pos = (current_pos.0 + 1, current_pos.1);
                current_velocity = Velocity::East;
            }
            (Tile::BendSE, Velocity::West) => {
                current_pos = (current_pos.0, current_pos.1 + 1);
                current_velocity = Velocity::South;
            }
            (Tile::BendSE, _) => panic!("This should never happen!"),
            (Tile::Ground, _) => panic!("This should never happen!"),
            (Tile::Start, _) => panic!("We should have left the loop before this point"),
        }
        current_tile = tile_grid
            .get(current_pos.1)
            .unwrap()
            .get(current_pos.0)
            .unwrap()
            .to_owned();
        if current_tile != Tile::Start {
            loop_tiles[current_pos.1][current_pos.0] = current_tile;
        }
    }
    let mut area: usize = 0;
    for line in loop_tiles.iter() {
        let mut in_loop = false;
        let mut last_sig_tile = Tile::Ground;
        for &tile in line.iter() {
            match (tile, last_sig_tile) {
                (Tile::Vertical, _) => in_loop = !in_loop,
                (Tile::Horizontal, _) => (),
                (Tile::BendNE, _) => last_sig_tile = Tile::BendNE,
                (Tile::BendNW, Tile::BendNE) => (),
                (Tile::BendNW, Tile::BendSE) => in_loop = !in_loop,
                (Tile::BendNW, _) => panic!("IDK WHAT TO DO HERE"),
                (Tile::BendSW, Tile::BendSE) => (),
                (Tile::BendSW, Tile::BendNE) => in_loop = !in_loop,
                (Tile::BendSW, _) => panic!("IDK WHAT TO DO HERE"),
                (Tile::BendSE, _) => last_sig_tile = Tile::BendSE,
                (Tile::Start, _) => panic!("IDK WHAT TO DO HERE"),
                (Tile::Ground, _) => {
                    if in_loop {
                        area += 1
                    }
                }
            }
        }
    }
    println!("Day 10 Part 2: {}", area);
}
