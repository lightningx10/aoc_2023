use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Instruction {
    Left,
    Right,
}

type Name = [char; 3];

//type Node = (Name, (Name, Name));

pub fn day8_p1(input: &str) {
    let mut node_map: HashMap<Name, (Name, Name)> = HashMap::new();

    let lines: Vec<&str> = input.lines().collect();
    let insts: Vec<Instruction> = lines[0]
        .chars()
        .map(|c| match c {
            'L' => Instruction::Left,
            'R' => Instruction::Right,
            _ => panic!("Invalid instruction in the instruction string!"),
        })
        .collect();

    for &line in lines.iter().skip(2) {
        let (name_str, dests_str) = line.split_once(" = ").unwrap();
        let node_name: Name = name_str.chars().collect::<Vec<char>>().try_into().unwrap();
        let (left_str, right_str) = dests_str.split_once(", ").unwrap();
        let left_name: Name = left_str
            .chars()
            .filter(|c| *c != '(')
            .collect::<Vec<char>>()
            .try_into()
            .unwrap();
        let right_name: Name = right_str
            .chars()
            .filter(|c| *c != ')')
            .collect::<Vec<char>>()
            .try_into()
            .unwrap();
        node_map.insert(node_name, (left_name, right_name));
    }
    let mut current_node: Name = ['A', 'A', 'A'];
    let mut i: usize = 0;
    let n_insts = insts.len();
    while current_node != ['Z', 'Z', 'Z'] {
        current_node = match insts[i % n_insts] {
            Instruction::Left => node_map.get(&current_node).unwrap().0,
            Instruction::Right => node_map.get(&current_node).unwrap().1,
        };
        i += 1;
    }
    println!("Day 8 Part 1: {}", i);
}

pub fn day8_p2(input: &str) {
    let mut node_map: HashMap<Name, (Name, Name)> = HashMap::new();

    let lines: Vec<&str> = input.lines().collect();
    let insts: Vec<Instruction> = lines[0]
        .chars()
        .map(|c| match c {
            'L' => Instruction::Left,
            'R' => Instruction::Right,
            _ => panic!("Invalid instruction in the instruction string!"),
        })
        .collect();

    for &line in lines.iter().skip(2) {
        let (name_str, dests_str) = line.split_once(" = ").unwrap();
        let node_name: Name = name_str.chars().collect::<Vec<char>>().try_into().unwrap();
        let (left_str, right_str) = dests_str.split_once(", ").unwrap();
        let left_name: Name = left_str
            .chars()
            .filter(|c| *c != '(')
            .collect::<Vec<char>>()
            .try_into()
            .unwrap();
        let right_name: Name = right_str
            .chars()
            .filter(|c| *c != ')')
            .collect::<Vec<char>>()
            .try_into()
            .unwrap();
        node_map.insert(node_name, (left_name, right_name));
    }

    let initial_states: Vec<Name> = node_map
        .keys()
        .filter(|n| n[2] == 'A')
        .map(|n| n.to_owned())
        .collect();

    let ns: Vec<usize> = initial_states
        .iter()
        .map(|n| {
            let mut current_node: Name = *n;
            let mut i: usize = 0;
            let n_insts = insts.len();
            while current_node[2] != 'Z' {
                current_node = match insts[i % n_insts] {
                    Instruction::Left => node_map.get(&current_node).unwrap().0,
                    Instruction::Right => node_map.get(&current_node).unwrap().1,
                };
                i += 1;
            }
            i
        })
        .collect();
    let lcm: usize = ns.iter().fold(1, |lcm, &n| num::integer::lcm(lcm, n));
    println!("Day 8 Part 2: {}", lcm);
}
