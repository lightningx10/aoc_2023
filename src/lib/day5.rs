pub fn day5_p1(input: &str) {
    let inputs: Vec<&str> = input.split("\n\n").collect();
    let seeds_str = inputs[0];
    let seed_to_soil_map_str = inputs[1];
    let soil_to_fertilizer_map_str = inputs[2];
    let fertilizer_to_water_map_str = inputs[3];
    let water_to_light_map_str = inputs[4];
    let light_to_temperature_map_str = inputs[5];
    let temperature_to_humidity_map_str = inputs[6];
    let humidity_to_location_map_str = inputs[7];

    let seeds: Vec<usize> = seeds_str
        .chars()
        .skip(6)
        .collect::<String>()
        .split(" ")
        .filter_map(|s| usize::from_str_radix(s, 10).ok())
        .collect();

    let mut seed_to_soil_map = Vec::new();
    let mut soil_to_fertilizer_map = Vec::new();
    let mut fertilizer_to_water_map = Vec::new();
    let mut water_to_light_map = Vec::new();
    let mut light_to_temperature_map = Vec::new();
    let mut temperature_to_humidity_map = Vec::new();
    let mut humidity_to_location_map = Vec::new();

    let mapper = |map_str: &str, map: &mut Vec<(usize, usize, usize)>| {
        for line in map_str 
            .lines()
            .filter(|line| line.len() != 0)
            .skip(1) {
                let ns: Vec<usize> = line.split(" ").filter_map(|s| usize::from_str_radix(s, 10).ok()).collect();
                let dst_rng_start = ns[0];
                let src_rng_start = ns[1];
                let rng_length = ns[2];
                map.push((dst_rng_start, src_rng_start, rng_length));
            }
    };

    mapper(seed_to_soil_map_str, &mut seed_to_soil_map);
    mapper(soil_to_fertilizer_map_str, &mut soil_to_fertilizer_map);
    mapper(fertilizer_to_water_map_str, &mut fertilizer_to_water_map);
    mapper(water_to_light_map_str, &mut water_to_light_map);
    mapper(light_to_temperature_map_str, &mut light_to_temperature_map);
    mapper(temperature_to_humidity_map_str, &mut temperature_to_humidity_map);
    mapper(humidity_to_location_map_str, &mut humidity_to_location_map);

    let maps = [
        ("Seed->soil", &seed_to_soil_map),
        ("Soil->Fertilizer", &soil_to_fertilizer_map),
        ("Fertilizer->Water", &fertilizer_to_water_map),
        ("Water->Light", &water_to_light_map),
        ("Light->Temperature", &light_to_temperature_map),
        ("Temperature->Humidity", &temperature_to_humidity_map),
        ("Humidity->Location", &humidity_to_location_map)
    ];

    let locations: Vec<usize> = seeds
        .iter()
        .map(|seed| {
            let mut id = *seed;
            for (_, map) in maps {
                for &(to, from, range) in map {
                    if id >= from && id < from + range {
                        id = to + id - from;
                        break
                    }
                }
            }
            id
        })
        .collect();

    println!("{:?}", locations);
    println!("Day 5 Part 1: {}", locations.iter().fold(locations[0], |min, n| min.min(*n)));
}

pub fn day5_p2(input: &str) {
    let inputs: Vec<&str> = input.split("\n\n").collect();
    let seeds_str = inputs[0];
    let seed_to_soil_map_str = inputs[1];
    let soil_to_fertilizer_map_str = inputs[2];
    let fertilizer_to_water_map_str = inputs[3];
    let water_to_light_map_str = inputs[4];
    let light_to_temperature_map_str = inputs[5];
    let temperature_to_humidity_map_str = inputs[6];
    let humidity_to_location_map_str = inputs[7];

    let seed_ranges: Vec<(usize, usize)> = seeds_str
        .chars()
        .skip(6)
        .collect::<String>()
        .split(" ")
        .filter_map(|s| usize::from_str_radix(s, 10).ok())
        .collect::<Vec<_>>()
        .chunks(2)
        .map(|vs| (vs[0], vs[1]))
        .collect();

    let mut seed_to_soil_map = Vec::new();
    let mut soil_to_fertilizer_map = Vec::new();
    let mut fertilizer_to_water_map = Vec::new();
    let mut water_to_light_map = Vec::new();
    let mut light_to_temperature_map = Vec::new();
    let mut temperature_to_humidity_map = Vec::new();
    let mut humidity_to_location_map = Vec::new();

    let mapper = |map_str: &str, map: &mut Vec<(usize, usize, usize)>| {
        for line in map_str 
            .lines()
            .filter(|line| line.len() != 0)
            .skip(1) {
                let ns: Vec<usize> = line.split(" ").filter_map(|s| usize::from_str_radix(s, 10).ok()).collect();
                let dst_rng_start = ns[0];
                let src_rng_start = ns[1];
                let rng_length = ns[2];
                map.push((dst_rng_start, src_rng_start, rng_length));
            }
    };

    mapper(seed_to_soil_map_str, &mut seed_to_soil_map);
    mapper(soil_to_fertilizer_map_str, &mut soil_to_fertilizer_map);
    mapper(fertilizer_to_water_map_str, &mut fertilizer_to_water_map);
    mapper(water_to_light_map_str, &mut water_to_light_map);
    mapper(light_to_temperature_map_str, &mut light_to_temperature_map);
    mapper(temperature_to_humidity_map_str, &mut temperature_to_humidity_map);
    mapper(humidity_to_location_map_str, &mut humidity_to_location_map);

    // This brute-forcer is too slow, took around an hour to terminate lol
    // Do I care? No
    let maps = [
        ("Seed->soil", &seed_to_soil_map),
        ("Soil->Fertilizer", &soil_to_fertilizer_map),
        ("Fertilizer->Water", &fertilizer_to_water_map),
        ("Water->Light", &water_to_light_map),
        ("Light->Temperature", &light_to_temperature_map),
        ("Temperature->Humidity", &temperature_to_humidity_map),
        ("Humidity->Location", &humidity_to_location_map)
    ];

    let mut locations: Vec<usize> = Vec::new();
    for (start, range) in seed_ranges {
        let ubound = start + range;
        for seed in start..ubound {
            let mut id = seed;
            for (_, map) in maps {
                for &(to, from, range) in map {
                    if id >= from && id < from + range {
                        id = to + id - from;
                        break
                    }
                }
            }
            locations.push(id);
        }
    }

    println!("Day 5 Part 2: {}", locations.iter().fold(locations[0], |min, n| min.min(*n)));
}
