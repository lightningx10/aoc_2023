pub fn day6_p1(input: &str) {
    let lines: Vec<&str> = input.lines().collect();
    let times: Vec<usize> = lines[0].split(" ").filter_map(|s| usize::from_str_radix(s, 10).ok()).collect();
    let dists: Vec<usize> = lines[1].split(" ").filter_map(|s| usize::from_str_radix(s, 10).ok()).collect();
    assert_eq!(times.len(), dists.len());
    let mut prod = 1;
    for (time, dist_to_beat) in times.clone().into_iter().zip(dists) {
        let mut num_ways_to_beat = 0;
        for charge_time in 1..time {
            let velocity = charge_time;
            let dist_covered = (time - charge_time) * velocity;
            if dist_covered > dist_to_beat {
                num_ways_to_beat += 1;
            }
        }
        prod *= num_ways_to_beat;
    }
    println!("Day 6 Part 1: {}", prod);
}

pub fn day6_p2(input: &str) {
    let lines: Vec<&str> = input.lines().collect();
    let time_str = lines[0].chars().filter(|c| c.is_numeric()).collect::<String>();
    let record_str = lines[1].chars().filter(|c| c.is_numeric()).collect::<String>();
    let time = usize::from_str_radix(&time_str, 10).unwrap();
    let record = usize::from_str_radix(&record_str, 10).unwrap();
    let mut num_ways_to_beat = 0;
    for charge_time in 1..time {
        let velocity = charge_time;
        let dist_covered = (time - charge_time) * velocity;
        if dist_covered > record {
            num_ways_to_beat += 1;
        }
    }
    println!("Day 6 Part 2: {}", num_ways_to_beat)
}
