use std::collections::{HashMap, HashSet};

#[derive(Clone, Copy, Debug)]
enum Component {
    Num(usize, bool),
    Symb(char),
}

pub fn day3_p1(input: &str) {
    let mut components: HashMap<(usize, usize), Component> = HashMap::new();
    for (line_no, line) in input.lines().enumerate() {
        let ns: Vec<usize> = line
            .split(|c| !(c >= '0' && c <= '9'))
            .map(|s| s.chars().filter(|&c| c >= '0' && c <= '9').collect::<String>())
            .filter_map(|s| usize::from_str_radix(&s, 10).ok())
            .collect();
        let mut in_n = false;
        let n_ps: Vec<usize> = line
            .chars()
            .enumerate()
            .filter_map(|(index, c)| {
                if c >= '0' && c <= '9' && in_n {
                    None
                } else if c >= '0' && c <= '9' && !in_n {
                    in_n = true;
                    Some(index)
                } else {
                    in_n = false;
                    None
                }
            })
            .collect();
        assert!(ns.len() == n_ps.len(), "ns = {:?}, n_ps = {:?}, lineno = {}", ns, n_ps, line_no);
        let syms: Vec<(usize, char)> = line
            .chars()
            .enumerate()
            .filter(|(_, c)| !(*c >= '0' && *c <= '9') && *c != '.')
            .collect();
        for (&i, n) in n_ps.iter().zip(ns) {
            components.insert((i, line_no), Component::Num(n, false));
        }
        for &(i, sym) in syms.iter() {
            components.insert((i, line_no), Component::Symb(sym));
        }
    }
    for ((x, y), component) in components.clone().into_iter() {
        match component {
            Component::Num(n, false) => {
                let str_len = n.to_string().len();
                let x_low = if x == 0 { x } else { x - 1 };
                let x_up = x + str_len;
                let y_low = if y == 0 { y } else { y - 1 };
                let y_up = y + 1;

                for o_x in x_low..=x_up {
                    for o_y in y_low..=y_up {
                        match components.get(&(o_x, o_y)) {
                            Some(Component::Symb(_)) => {
                                //println!("Part no: {}", n);
                                components.insert((x, y), Component::Num(n, true));
                            }
                            _       => ()
                        }
                    }
                }
            }
            _ => ()
        }
    }
    let sum: usize = components
        .iter()
        .filter_map(|(_, comp)| {
            if let Component::Num(n, true) = comp {
                Some(n)
            } else {
                None
            }
        })
        .fold(0, |acc, &n| acc + n);
    println!("Day 3 Part 1: {}", sum);
}

pub fn day3_p2(input: &str) {
    let mut components: HashMap<(usize, usize), Component> = HashMap::new();
    for (line_no, line) in input.lines().enumerate() {
        let ns: Vec<usize> = line
            .split(|c| !(c >= '0' && c <= '9'))
            .map(|s| s.chars().filter(|&c| c >= '0' && c <= '9').collect::<String>())
            .filter_map(|s| usize::from_str_radix(&s, 10).ok())
            .collect();
        let mut in_n = false;
        let n_ps: Vec<usize> = line
            .chars()
            .enumerate()
            .filter_map(|(index, c)| {
                if c >= '0' && c <= '9' && in_n {
                    None
                } else if c >= '0' && c <= '9' && !in_n {
                    in_n = true;
                    Some(index)
                } else {
                    in_n = false;
                    None
                }
            })
            .collect();
        assert!(ns.len() == n_ps.len(), "ns = {:?}, n_ps = {:?}, lineno = {}", ns, n_ps, line_no);
        let syms: Vec<(usize, char)> = line
            .chars()
            .enumerate()
            .filter(|(_, c)| !(*c >= '0' && *c <= '9') && *c != '.')
            .collect();
        for (&i, n) in n_ps.iter().zip(ns) {
            for j in 0..(n.to_string().len()) {
                components.insert((i + j, line_no), Component::Num(n, false));
            }
        }
        for &(i, sym) in syms.iter() {
            components.insert((i, line_no), Component::Symb(sym));
        }
    }

    let mut sum: usize = 0;
    for ((x, y), component) in components.clone() {
        match component {
            Component::Symb('*') => {
                let mut adjacent: HashSet<usize> = HashSet::new();
                let x_low = if x == 0 {x} else {x-1};
                let x_up = x + 1;
                let y_low = if y == 0 {y} else {y-1};
                let y_up = y + 1;

                for o_x in x_low..=x_up {
                    for o_y in y_low..=y_up {
                        match components.get(&(o_x, o_y)) {
                            Some(Component::Num(n, _)) => {
                                //println!("Found {} adjacent", n);
                                adjacent.insert(*n);
                            }
                            _ => ()
                        }
                    }
                }
                if adjacent.len() == 2 {
                    sum += adjacent.iter().fold(1, |acc, &n| acc * n);
                }
            }
            _ => ()
        }
    }
    println!("Day 3 Part 2: {}", sum);
}
