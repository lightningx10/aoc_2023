use core::hash::Hash;
use std::{collections::HashSet, fmt::Display};

#[derive(Eq, PartialEq, Copy, Clone)]
enum Space {
    Empty,
    Galaxy,
    Expansion,
}

impl From<char> for Space {
    fn from(c: char) -> Self {
        match c {
            '.' => Space::Empty,
            '#' => Space::Galaxy,
            _ => Space::Empty,
        }
    }
}

impl Display for Space {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Space::Empty => '.',
                Space::Galaxy => '#',
                Space::Expansion => 'X',
            }
        )
    }
}

#[derive(Copy, Clone)]
struct Pair {
    item1: (usize, usize),
    item2: (usize, usize),
}

impl From<Pair> for usize {
    fn from(value: Pair) -> Self {
        value.item1.0.abs_diff(value.item2.0) + value.item1.1.abs_diff(value.item2.1)
    }
}

impl PartialEq for Pair {
    fn eq(&self, other: &Self) -> bool {
        (self.item1 == other.item1 && self.item2 == other.item2)
            || (self.item1 == other.item2 && self.item2 == other.item1)
    }
}

impl Eq for Pair {}

impl Hash for Pair {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        if self.item1 < self.item2 {
            state.write_usize(self.item1.0);
            state.write_usize(self.item1.1);
            state.write_usize(self.item2.0);
            state.write_usize(self.item2.1);
        } else {
            state.write_usize(self.item2.0);
            state.write_usize(self.item2.1);
            state.write_usize(self.item1.0);
            state.write_usize(self.item1.1);
        }
    }
}

pub fn day11_p1(input: &str) {
    let mut universe: Vec<Vec<Space>> = input
        .lines()
        .map(|l| l.chars().map(Space::from).collect())
        .collect();
    let mut num_expansions: usize = 0;
    for (i, line) in universe.clone().into_iter().enumerate() {
        if line.iter().all(|&s| s == Space::Empty) {
            universe.insert(i + num_expansions, line);
            num_expansions += 1;
        }
    }
    let mut universe_transpose: Vec<Vec<Space>> = (0..universe[0].len())
        .map(|i| universe.iter().map(|l| l[i]).collect())
        .collect();
    num_expansions = 0;
    for (i, line) in universe_transpose.clone().into_iter().enumerate() {
        if line.iter().all(|&s| s == Space::Empty) {
            universe_transpose.insert(i + num_expansions, line);
            num_expansions += 1;
        }
    }
    let universe_final: Vec<Vec<Space>> = (0..universe_transpose[0].len())
        .map(|i| universe_transpose.iter().map(|l| l[i]).collect())
        .collect();
    let mut pairs: HashSet<Pair> = HashSet::new();
    let galaxies: HashSet<(usize, usize)> = universe_final
        .iter()
        .enumerate()
        .flat_map(|(i, l)| {
            l.iter()
                .enumerate()
                .filter(|(_, s)| **s != Space::Empty)
                .map(move |(j, _)| (i, j))
        })
        .collect();
    for &galaxy in galaxies.iter() {
        for &othergalaxy in galaxies.iter() {
            if galaxy != othergalaxy {
                let pair: Pair = Pair {
                    item1: galaxy,
                    item2: othergalaxy,
                };
                pairs.insert(pair);
            }
        }
    }
    let result = pairs.iter().fold(0, |acc, &p| acc + usize::from(p));
    println!("Day 11 Part 1: {}", result);
}

pub fn day11_p2(input: &str) {
    let mut universe: Vec<Vec<Space>> = input
        .lines()
        .map(|l| l.chars().map(Space::from).collect())
        .collect();
    for (i, line) in universe.clone().into_iter().enumerate() {
        if line.iter().all(|&s| s == Space::Empty) {
            universe[i] = vec![Space::Expansion; line.len()];
        }
    }
    let mut universe_transpose: Vec<Vec<Space>> = (0..universe[0].len())
        .map(|i| universe.iter().map(|l| l[i]).collect())
        .collect();
    for (i, line) in universe_transpose.clone().into_iter().enumerate() {
        if line
            .iter()
            .all(|&s| s == Space::Empty || s == Space::Expansion)
        {
            universe_transpose[i] = vec![Space::Expansion; line.len()];
        }
    }
    let universe_final: Vec<Vec<Space>> = (0..universe_transpose[0].len())
        .map(|i| universe_transpose.iter().map(|l| l[i]).collect())
        .collect();

    let mut galaxies: HashSet<(usize, usize)> = HashSet::new();

    let mut x = 0;
    let mut y = 0;

    for line in universe_final {
        if line.iter().all(|&s| s == Space::Expansion) {
            y += 1_000_000;
        } else {
            for space in line {
                match space {
                    Space::Galaxy => {
                        galaxies.insert((x, y));
                        x += 1;
                    }
                    Space::Expansion => x += 1_000_000,
                    Space::Empty => x += 1,
                }
            }
            x = 0;
            y += 1;
        }
    }
    let pairs: HashSet<Pair> = galaxies
        .clone()
        .into_iter()
        .flat_map(|c1| {
            galaxies.iter().filter_map(move |&c2| {
                if c1 == c2 {
                    None
                } else {
                    Some(Pair {
                        item1: c1,
                        item2: c2,
                    })
                }
            })
        })
        .collect();

    let result = pairs.iter().fold(0, |acc, &p| acc + usize::from(p));

    println!("Day 11 Part 2: {}", result);
}
