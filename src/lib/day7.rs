use std::collections::HashMap;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace
}

type Hand = [Card; 5];

#[derive(PartialEq, Eq, Clone, Copy)]
enum HandType {
    HighCard(Hand),
    OnePair(Hand),
    TwoPair(Hand),
    ThreeOfAKind(Hand),
    FullHouse(Hand),
    FourOfAKind(Hand),
    FiveOfAKind(Hand)
}

impl Ord for HandType {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let rank_handtype = |htype: &HandType| match htype {
            Self::HighCard(_)     => 0,
            Self::OnePair(_)      => 1,
            Self::TwoPair(_)      => 2,
            Self::ThreeOfAKind(_) => 3,
            Self::FullHouse(_)    => 4,
            Self::FourOfAKind(_)  => 5,
            Self::FiveOfAKind(_)  => 6
        } as u8;
        let get_hand = |htype: &HandType| match htype {
            Self::HighCard(hand) => hand.clone(),
            Self::OnePair(hand) => hand.clone(),
            Self::TwoPair(hand) => hand.clone(),
            Self::ThreeOfAKind(hand) => hand.clone(),
            Self::FullHouse(hand) => hand.clone(),
            Self::FourOfAKind(hand) => hand.clone(),
            Self::FiveOfAKind(hand) => hand.clone(),
        };
        let rank = rank_handtype(self);
        let rank_other = rank_handtype(other);
        let hand_self = get_hand(self);
        let hand_other = get_hand(other);

        match (rank == rank_other, rank > rank_other) {
            (true, _) => {
                hand_self.into_iter().zip(hand_other).filter(|(ours, theirs)| ours.ne(theirs)).map(|(ours, theirs)| ours.cmp(&theirs)).next().unwrap_or(std::cmp::Ordering::Equal)
            }
            (_, true) => std::cmp::Ordering::Greater,
            (_, false) => std::cmp::Ordering::Less
        }
    }
}

impl PartialOrd for HandType {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

pub fn day7_p1(input: &str) {
    let mut hands: Vec<(HandType, usize)> = input.lines().map(|line| {
        let split: Vec<&str> = line.split(" ").collect();
        let hand_str = split[0];
        let bet: usize = usize::from_str_radix(split[1], 10).unwrap();

        let hand: Hand = match &hand_str.chars().map(|c| match c {
            'A' => Card::Ace,
            'K' => Card::King,
            'Q' => Card::Queen,
            'J' => Card::Jack,
            'T' => Card::Ten,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            _   => panic!("Invalid input hand")
        })
            .collect::<Vec<Card>>()[..] {
                &[c1, c2, c3, c4, c5] => [c1, c2, c3, c4, c5],
                _ => panic!("Not enough cards in hand!")
            };
        let mut card_map: HashMap<Card, usize> = HashMap::new();
        for card in hand.iter() {
            let n = *card_map.get(card).unwrap_or(&0) + 1;
            card_map.insert(*card, n);
        }
        let max_n = card_map.iter().fold(0, |acc: usize, (_, &n)| n.max(acc));
        (match card_map.len() {
            5 => HandType::HighCard(hand),
            4 => HandType::OnePair(hand),
            3 => {
                match max_n {
                    2 => HandType::TwoPair(hand),
                    3 => HandType::ThreeOfAKind(hand),
                    _ => panic!("This should be impossible")
                }
            }
            2 => {
                match max_n {
                    3 => HandType::FullHouse(hand),
                    4 => HandType::FourOfAKind(hand),
                    _ => panic!("This should be impossible")
                }
            }
            1 => HandType::FiveOfAKind(hand),
            _ => panic!("Hand too big!")
        }, bet)
    }).collect();

    hands.sort_by(|(h1, _), (h2, _)| h1.cmp(h2));
    let total = hands.iter().enumerate().fold(0, |acc, (i, (_, b))| acc + b * (i + 1));
    println!("Day 7 Part 1: {}", total)
}

pub fn day7_p2(input: &str) {
    let mut hands: Vec<(HandType, usize)> = input.lines().map(|line| {
        let split: Vec<&str> = line.split(" ").collect();
        let hand_str = split[0];
        let bet: usize = usize::from_str_radix(split[1], 10).unwrap();

        let hand: Hand = match &hand_str.chars().map(|c| match c {
            'A' => Card::Ace,
            'K' => Card::King,
            'Q' => Card::Queen,
            'J' => Card::Joker,
            'T' => Card::Ten,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            _   => panic!("Invalid input hand")
        })
            .collect::<Vec<Card>>()[..] {
                &[c1, c2, c3, c4, c5] => [c1, c2, c3, c4, c5],
                _ => panic!("Not enough cards in hand!")
            };
        let mut card_map: HashMap<Card, usize> = HashMap::new();
        let mut num_jokers: usize = 0;
        for card in hand.iter() {
            let n = *card_map.get(card).unwrap_or(&0) + 1;
            if card.eq(&Card::Joker) {
                num_jokers += 1;
            } else {
                card_map.insert(*card, n);
            }
        }
        let max_n = card_map.iter().fold(0, |acc: usize, (_, &n)| n.max(acc));
        (match num_jokers {
            0 => match card_map.len() {
                5 => HandType::HighCard(hand),
                4 => HandType::OnePair(hand),
                3 => {
                    match max_n {
                        2 => HandType::TwoPair(hand),
                        3 => HandType::ThreeOfAKind(hand),
                        _ => panic!("This should be impossible")
                    }
                }
                2 => {
                    match max_n {
                        3 => HandType::FullHouse(hand),
                        4 => HandType::FourOfAKind(hand),
                        _ => panic!("This should be impossible")
                    }
                }
                1 => HandType::FiveOfAKind(hand),
                _ => panic!("Hand too big!")
            }
            1 => match card_map.len() {
                4 => HandType::OnePair(hand),
                3 => HandType::ThreeOfAKind(hand),
                2 => {
                    // There could be either three of a kind and one
                    // or there's two pairs
                    match max_n {
                        2 => HandType::FullHouse(hand),
                        3 => HandType::FourOfAKind(hand),
                        _ => panic!("This should be impossible")
                    }
                }
                1 => HandType::FiveOfAKind(hand),
                _ => panic!("This should be impossible")
            }
            2 => match card_map.len() {
                3 => HandType::ThreeOfAKind(hand),
                2 => HandType::FourOfAKind(hand),
                1 => HandType::FiveOfAKind(hand),
                _ => panic!("This should be impossible")
            }
            3 => match card_map.len() {
                2 => HandType::FourOfAKind(hand),
                1 => HandType::FiveOfAKind(hand),
                _ => panic!("This should be impossible")
            }
            4 => HandType::FiveOfAKind(hand),
            5 => HandType::FiveOfAKind(hand),
            _ => panic!("This shouldn't happen")
        }, bet)
    }).collect();

    hands.sort_by(|(h1, _), (h2, _)| h1.cmp(h2));
    let total = hands.iter().enumerate().fold(0, |acc, (i, (_, b))| acc + b * (i + 1));
    println!("Day 7 Part 2: {}", total)
}
